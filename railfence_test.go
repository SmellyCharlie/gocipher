package gocipher

import (
	"bytes"
	"testing"
)

func TestRailfence(t *testing.T) {
	testCases := []struct {
		p   []byte
		c   []byte
		key int
	}{
		{
			[]byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"),
			[]byte("TQKOFJSEHADH UC RW O UP VRTELZ OEIBNXMO  YG"),
			3,
		},
		{
			[]byte("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG"),
			[]byte("TUBNJSRLDHQIKRWFXUPOETEAYOECOOMVHZG"),
			3,
		},
	}

	for n, tc := range testCases {
		if c := RailfenceEncrypt(tc.p, tc.key); !bytes.Equal(c, tc.c) {
			t.Fatalf("Encrypt %d failed: got %s, want %s", n, c, tc.c)
		}

		if c := RailfenceDecrypt(tc.c, tc.key); !bytes.Equal(c, tc.p) {
			t.Fatalf("Decrypt %d failed: got %s, want %s", n, c, tc.p)
		}
	}

}

func BenchmarkRailfenceEncrypt(b *testing.B) {
	pt := []byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG")
	key := 3
	for i := 0; i < b.N; i++ {
		RailfenceEncrypt(pt, key)
	}
}

func BenchmarkRailfenceDecrypt(b *testing.B) {
	ct := []byte("TQKOFJSEHADH UC RW O UP VRTELZ OEIBNXMO  YG")
	key := 3
	for i := 0; i < b.N; i++ {
		RailfenceDecrypt(ct, key)
	}
}
