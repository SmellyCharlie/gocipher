package gocipher

import "bytes"

// AffineEncrypt ...
func AffineEncrypt(pt []byte, a, b int) (ct []byte) {
	ct = bytes.ToUpper(pt)

	for i, char := range ct {
		if isAlpha(char) {
			ct[i] = itob(a*btoi(char) + b)
		}
	}
	return
}

// AffineDecrypt ...
func AffineDecrypt(ct []byte, a, b int) (pt []byte) {
	pt = bytes.ToUpper(ct)

	inva := -1
	for i := 1; i < 26; i += 2 {
		if (a*i)%26 == 1 {
			inva = i
		}
	}

	for i, char := range ct {
		if isAlpha(char) {
			pt[i] = itob(inva * (btoi(char) - b))
		}
	}
	return
}
