package gocipher

import (
	"bytes"
	"testing"
)

func TestROT13(t *testing.T) {
	testCases := []struct {
		p []byte
		c []byte
	}{
		{
			[]byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"),
			[]byte("GUR DHVPX OEBJA SBK WHZCF BIRE GUR YNML QBT"),
		},
		{
			[]byte("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG"),
			[]byte("GURDHVPXOEBJASBKWHZCFBIREGURYNMLQBT"),
		},
	}

	for _, tc := range testCases {
		if c := ROT13Encrypt(tc.p); !bytes.Equal(c, tc.c) {
			t.Fatalf("Encrypt failed: got %s, want %s", c, tc.c)
		}

		if c := ROT13Decrypt(tc.c); !bytes.Equal(c, tc.p) {
			t.Fatalf("Decrypt failed: got %s, want %s", c, tc.p)
		}
	}

}

func BenchmarkROT13Encrypt(b *testing.B) {
	pt := []byte("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG")
	for i := 0; i < b.N; i++ {
		ROT13Encrypt(pt)
	}
}

func BenchmarkROT13Decrypt(b *testing.B) {
	ct := []byte("ASD LFXTH OQBPW IBU CFRGV BKDQ ASD MJEZ YBN")
	for i := 0; i < b.N; i++ {
		ROT13Decrypt(ct)
	}
}
