package gocipher

import (
	"bytes"
)

// RailfenceEncrypt ...
func RailfenceEncrypt(pt []byte, key int) (ct []byte) {
	pt = bytes.ToUpper(pt)
	ct = make([]byte, len(pt))
	for i, char := range buildFence(pt, key) {
		ct[i] = char.(byte)
	}

	return

}

// RailfenceDecrypt ...
func RailfenceDecrypt(ct []byte, key int) (pt []byte) {
	ct = bytes.ToUpper(ct)

	ind := make([]int, 0, len(ct))
	for i := 0; i < len(ct); i++ {
		ind = append(ind, i)
	}

	fence := buildFence(ind, key)
	pt = make([]byte, 0, len(ct))
	for i := range ind {
		pt = append(pt, ct[indexOf(i, fence)])
	}

	return
}

func buildFence(in interface{}, key int) []interface{} {
	rails := rails(key)
	fence := make([][]interface{}, key)

	switch in.(type) {
	case []byte:
		buf := in.([]byte)
		for i := 0; i < key; i++ {
			fence[i] = make([]interface{}, len(buf))
		}
		for i, char := range buf {
			fence[rails[i%len(rails)]][i] = char
		}

	case []int:
		buf := in.([]int)
		for i := 0; i < key; i++ {
			fence[i] = make([]interface{}, len(buf))
		}

		for i, char := range buf {
			fence[rails[i%len(rails)]][i] = char
		}
	}

	out := make([]interface{}, 0)
	for _, r := range fence {
		for _, c := range r {
			if c != nil {
				out = append(out, c)
			}
		}
	}

	return out
}

func indexOf(n int, data []interface{}) int {
	for k, v := range data {
		if n == v {
			return k
		}
	}
	return -1
}

func rails(key int) (rails []int) {
	rails = make([]int, 0)
	for i := 0; i < key-1; i++ {
		rails = append(rails, i)
	}

	for i := key - 1; i > 0; i-- {
		rails = append(rails, i)
	}
	return
}
