package gocipher

import (
	"bytes"
)

type PolybiusSquare struct {
	key   []byte
	chars []byte
	size  int
}

func NewPolybiusSquare(key []byte, size int, chars []byte) *PolybiusSquare {
	ps := &PolybiusSquare{
		size: size,
	}
	ps.key = bytes.ToUpper(key)

	if len(chars) == 0 {
		ps.chars = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ")[:size]
	} else {
		ps.chars = chars
	}

	return ps
}

func (c *PolybiusSquare) encryptChar(b byte) []byte {
	pos := bytes.Index(c.key, []byte{b})
	row := pos / c.size
	col := pos % c.size
	return []byte{c.chars[row], c.chars[col]}
}

func (c *PolybiusSquare) decryptPair(b []byte) byte {
	row := bytes.Index(c.chars, []byte{b[0]})
	col := bytes.Index(c.chars, []byte{b[1]})
	return c.key[row*c.size+col]
}

// Encrypt ...
func (c *PolybiusSquare) Encrypt(pt []byte) (ct []byte) {
	pt = bytes.Replace(bytes.ToUpper(pt), []byte(" "), []byte(""), -1)

	ct = make([]byte, 0)
	for _, char := range pt {
		ct = append(ct, c.encryptChar(char)...)
	}
	return
}

// Decrypt ...
func (c *PolybiusSquare) Decrypt(ct []byte) (pt []byte) {
	ct = bytes.Replace(bytes.ToUpper(ct), []byte(" "), []byte(""), -1)

	pt = make([]byte, 0)
	for i := 0; i < len(ct); i += 2 {
		pt = append(pt, c.decryptPair(ct[i:i+2]))
	}
	return
}
