package gocipher

import "bytes"

// AtbashEncrypt ...
func AtbashEncrypt(pt []byte) (ct []byte) {
	ct = bytes.ToUpper(pt)
	for i, char := range ct {
		if isAlpha(char) {
			ct[i] = itob(25 - btoi(char))
		}
	}
	return ct
}

// AtbashDecrypt ...
func AtbashDecrypt(ct []byte) (pt []byte) {
	return AtbashEncrypt(ct)
}
